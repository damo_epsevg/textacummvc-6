package edu.upc.damo;

/**
 * Created by Josep M on 13/10/2014.
 */
public class ModelVisualitzable implements Vista.Visualitzable
{
    Model model;

    ModelVisualitzable(Model model){
        this.model = model;
    }

    @Override
    public int quants() {
        int i = 0;
        for(CharSequence s:model)
            i++;
        return i;
    }

    @Override
    public Object elem(int i) {
        int j = 0;
        for(CharSequence s:model) {
            if (j == i)
                return s;
            j++;
        }
        return null;

    }
}
